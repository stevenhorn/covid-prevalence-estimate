import json

configFilePath = '../config/config.json'

countriesToIncludeParentId = ["Canada"]
provinceAbbreviations = {
    "British Columbia": "BC",
    "Newfoundland and Labrador": "NL",
}

data = {}
with open(configFilePath, 'r') as f:
    data = json.load(f)

parentNameIdDict = {
    "Vancouver Island": "CanadaBCIsland"
}
populations = data['populations']
for entry in populations:
    sourceCountry = entry["source_country"]
    if sourceCountry in countriesToIncludeParentId and not entry["source_region"]:
        sourceState = entry["source_state"]
        abbrSourceState = sourceState
        if sourceState in provinceAbbreviations:
            abbrSourceState = provinceAbbreviations[sourceState]
        parentNameId = sourceCountry + sourceState
        parentNameId = parentNameId.replace(' ', '')
        parentNameIdDict[abbrSourceState] = parentNameId

for entry in populations:
    parentNameId = None
    sourceState = entry["source_state"]
    if sourceState in parentNameIdDict and entry["source_region"]:
        parentNameId = parentNameIdDict[sourceState]
    entry["parentNameId"] = parentNameId

with open(configFilePath, "w") as outfile:
    json.dump(data, outfile, indent=2, ensure_ascii=False)
