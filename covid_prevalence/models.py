"""
    COVID-Prevalence  Copyright (c) Her Majesty the Queen in Right of Canada, 
    as represented by the Minister of National Defence, 2020.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import numpy as np
import pandas as pd
import pymc3 as pm
import scipy.stats as stats
import datetime
import theano.tensor as tt
import theano
from math import exp
from dateutil.parser import isoparse
from covid19_inference.model import Cov19Model
import covid19_inference as cov19

log = logging.getLogger(__name__)


def dynamicChangePoints(pop, model_settings):
    """Time dependent spreading rate"""

    if "date_start" in pop:
        bd = isoparse(pop["date_start"])  # This is the first day of data

    elif "default_date_start" in model_settings:
        bd = isoparse(model_settings["default_date_start"])  # This is the first day of data

    daystep = pop["daystep_lambda"]
    delta = datetime.datetime.utcnow() - bd
    change_points_d2 = []

    for dd in np.arange(daystep, delta.days, daystep, dtype="int"):
        change_points_d2 += [
            dict(  # Fit the end
                pr_mean_date_transient=bd + datetime.timedelta(days=int(dd)),
                pr_median_transient_len=daystep / 2,  # how fast is this transition?
                #pr_sigma_transient_len=0.5,  # uncertainty how long to apply
                #pr_sigma_date_transient=1e-3,  # uncertainty when applied
                pr_sigma_transient_len=1e-6,  # uncertainty how long to apply
                pr_sigma_date_transient=1e-6,  # uncertainty when applied
                relative_to_previous=True,  # link spreading rate relative to previous time period
                pr_factor_to_previous=1,  # mean moves log this -> i.e. log(1) = 0+
                pr_median_lambda=0,  # normal offset rel to prev
                pr_sigma_lambda=0.2,
            )
        ]

    change_points = change_points_d2
    return change_points


def dynamicEin(model, new_cases, daystep=7, mean_import=2, sigma_import=2):
    """Dynamic infection import"""
    log.info("Generating dynamic prior for introduced infections (E_in_t)")
    Ein_list = []
    cases_first = new_cases[0:daystep].mean()
    if cases_first > 0:
        Ein_0 = pm.Normal(
            name=f"Ein_0", mu=tt.log(mean_import), sigma=tt.log(sigma_import)
        )
    else:
        Ein_0 = tt.constant(-1000, dtype="float64")  # ~0

    Ein_list.append(Ein_0)
    t_0 = model.sim_begin
    T_list = []
    delta = model.data_end - model.data_begin
    T_list.append(t_0)

    # For each daystep(7)-day period, add an rv
    for dd in np.arange(daystep, delta.days, daystep, dtype="int"):
        cases_next = new_cases[dd : dd + daystep * 2].mean()
        if cases_next > 0:
            Ein_d = pm.Normal(
                name=f"Ein_{dd}", mu=tt.log(mean_import), sigma=tt.log(sigma_import)
            )
        else:
            Ein_d = tt.constant(-1000, dtype="float64")  # ~0
        Ein_list.append(Ein_d)
        T_list.append(t_0 + datetime.timedelta(days=int(dd)))

    # Don't predict imported cases - no data in prediction leads to over estimation of exposed
    Ein_list.append(tt.constant(-1000, dtype="float64"))
    T_list.append(t_0 + datetime.timedelta(days=int(dd + daystep)))

    # Convert the values from the rv's to theano tensor
    T_list = np.array(T_list)
    Ein_list = np.array(Ein_list)
    Ein_t_list = []
    t = np.arange(model.sim_shape[0])
    for ix, ddays in enumerate(t):  # ddays = 10
        ix_date = t_0 + datetime.timedelta(days=int(ddays))
        Ein_dist = Ein_list[T_list <= ix_date][-1]
        Ein_ix = tt.zeros(model.sim_shape)
        Ein_ix = tt.set_subtensor(Ein_ix[ix], tt.exp(Ein_dist))
        Ein_t_list.append(Ein_ix)

    Ein_t_log = tt.log(sum(Ein_t_list))
    pm.Deterministic("Ein_t", tt.exp(Ein_t_log))
    return Ein_t_log


def SEIRa(
    lambda_t_log,  # spreading rate over time
    gamma,  # latent
    mu,  # time for asymptomatic recovery
    mus,  # time for symptomatic to show symptoms
    pa,  # asymptomatic probability distribution
    pu=0,  # under-reporting probability
    asym_ratio=1.0,  # relative infectiousness of asymptomatic cases
    name_new_I_t="new_I_t",
    name_I_begin="I_begin",
    pr_E_begin=20,
    pr_Ia_begin=20,
    pr_Is_begin=20,
    pr_prev_begin=0, #Starting prevalence estimate used to generate the Ia and Is prior means when using usePrevInit
    pr_mean_median_symptomatic=5,
    pr_sigma_median_symptomatic=1,
    sigma_isolate=0.3,
    lambda_max=None,
    Ein_t_log=None,  # Introduction of infections
    Ein_t_max=10,  # Maximum allowed imported cases per day
    useHNormInit=True,
    usePrevInit=False, #Activate the use of a prevalence prior for starting Ia, Is and E
    model=None,
    return_all=False,
    gamma_max=0.95,
    mus_max=0.95,
    mu_max=0.95,
):

    N = model.N_population  # Total number of people in population

    if Ein_t_log is None:  # Default is no introduction
        Ein_t_log = -1000 * tt.ones(model.sim_shape)  # computationally 0
        pm.Deterministic("Ein_t", tt.exp(Ein_t_log))

    lambda_t = tt.exp(lambda_t_log)
    Ein_t = tt.exp(Ein_t_log)

    gamma = tt.clip(gamma, 0, gamma_max)
    mus = tt.clip(mus, 0, mus_max)
    mu = tt.clip(mu, 0, mu_max)
    Ein_t = tt.clip(Ein_t, 0, Ein_t_max)

    # Prior distributions of starting populations (infectious, exposed, susceptibles)
    if useHNormInit:

        if usePrevInit:

            def ASE_begin_logp(values):
                return pm.HalfNormal.dist(sigma=pr_prev_begin).logp(values.sum()/N)
            ASE_begin = pm.DensityDist("ASE_begin", ASE_begin_logp, shape=3, testval=np.ones(3)*pr_prev_begin/6, transform=pm.distributions.transforms.log)
            Ia_begin = pm.Deterministic("Ia_begin", ASE_begin[0])
            Is_begin = pm.Deterministic("Is_begin", ASE_begin[1])
            E_begin = pm.Deterministic("E_begin", ASE_begin[2])

        else:
            Ia_begin = pm.HalfNormal(name="Ia_begin", sigma=pr_Ia_begin)
            Is_begin = pm.HalfNormal(name="Is_begin", sigma=pr_Is_begin)
            E_begin = pm.HalfNormal("E_begin", (Is_begin + Ia_begin) * lambda_t[0])
    else:

        if usePrevInit:

            def ASE_begin_logp(values):
                return pm.HalfCauchy.dist(beta=pr_prev_begin).logp(values.sum()/N)
            ASE_begin = pm.DensityDist("ASE_begin", ASE_begin_logp, shape=3, testval=np.ones(3)*pr_prev_begin/6, transform=pm.distributions.transforms.log)
            Ia_begin = pm.Deterministic("Ia_begin", ASE_begin[0])
            Is_begin = pm.Deterministic("Is_begin", ASE_begin[1])
            E_begin = pm.Deterministic("E_begin", ASE_begin[2])

        else:
            Ia_begin = pm.HalfCauchy(name="Ia_begin", beta=pr_Ia_begin)
            Is_begin = pm.HalfCauchy(name="Is_begin", beta=pr_Is_begin)
            E_begin = pm.HalfCauchy(name="E_begin", beta=pr_E_begin)
            E_begin = pm.HalfCauchy("E_begin", beta=(Is_begin + Ia_begin) * lambda_t[0])

    # Prior for susceptible
    S_begin = N - Ia_begin - Is_begin - E_begin

    new_I_0 = tt.zeros_like(Ia_begin)
    new_E_0 = tt.zeros_like(Ia_begin)

    # Runs SEIRa model:
    def next_day(
        lambda_t,  # infection rate
        Ein_t,  # imported exposures
        S_t,  # number of susceptible
        E_t,  # number of exposed
        E_cum_t,  # cumulative number exposed
        R_t,  # number of resolved
        Ia_t,  # number of asymptomatic-infected
        Is_t,  # number of pre-symptomatic infected
        _,  # not used
        ne,  # not used - for saving new exposures
        mu,  # recovery rate (asymptomatic)
        mus,
        gamma,  # incubation rate
        pa,  # probability asymptomatic
        pu,
        N,
    ):  # population size

        dt_per_day = 4
        lambda_t = tt.clip(lambda_t, 0, 3.0)  # prevent explosive growth

        pnodet = 1.0 - (1.0 - pa) * (1.0 - pu)
        detected=0
        new_E_t=0
        R_t=0

        for tick in np.arange(dt_per_day):
            # New Exposed from asymptomatic + presymptomatic
            new_E_t_tick = (Ein_t + lambda_t / N * (asym_ratio * Ia_t + Is_t) * S_t) / dt_per_day
            new_E_t += new_E_t_tick

            S_t = (
                S_t - new_E_t_tick + Ein_t / dt_per_day
            )  # Note, we add Ein here since these don't come from N

            # Exposed become infectious
            new_I_t = E_t * gamma / dt_per_day
            new_I_t = tt.clip(new_I_t, 0, E_t)  # stability

            E_t = E_t + new_E_t_tick - new_I_t

            # Note, we shouldn't count imported infections in the cumulative estimate.
            E_cum_t = E_cum_t + new_E_t_tick - Ein_t / dt_per_day

            # we assume only symptomatic + detected cases are reported (observed)
            # this is the variable used for Bayesian updates
            new_Is_t = (1 - pnodet) * new_I_t
            new_Ia_t = pnodet * new_I_t

            detected_tick = mus * Is_t / dt_per_day
            recovered_tick = mu * Ia_t / dt_per_day

            # we clip this since mu/mus could change to infeasible values during the
            # MCMC sampling process.  This way, we ensure that the model will be robust.
            detected_tick = tt.clip(detected_tick, 0, Is_t)  # stability
            recovered_tick = tt.clip(recovered_tick, 0, Ia_t)  # stability
            detected += detected_tick

            # distribute the new infections to be asymptomatic or symptomatic infectors
            Ia_t = Ia_t + new_Ia_t - recovered_tick  # recover as mu
            Is_t = Is_t + new_Is_t - detected_tick  # isolate as lognorm
            R_t_tick = recovered_tick + detected_tick  # resolved either recovered or isolated
            Ia_t = tt.clip(Ia_t, 0, N - 1)  # for stability
            Is_t = tt.clip(Is_t, 0, N - 1)  # for stability
            E_t = tt.clip(E_t, 0, N - 1)  # for stability
            S_t = tt.clip(S_t, 0, N)  # bound to population
            R_t_tick = tt.clip(R_t_tick, 0, N)  # bound to population
            R_t += R_t_tick

        return S_t, E_t, E_cum_t, R_t, Ia_t, Is_t, detected, new_E_t

    # theano scan returns two tuples, first one containing a time series of
    # what we give in outputs_info : S, I, new_I
    outputs, _ = theano.scan(
        fn=next_day,
        sequences=[lambda_t, Ein_t],
        outputs_info=[
            S_begin,
            E_begin,
            Ia_begin + Is_begin + E_begin,
            theano.shared(0.),
            Ia_begin,
            Is_begin,
            new_I_0,
            new_E_0,
        ],
        non_sequences=[mu, mus, gamma, pa, pu, N],
    )
    S_t, E_t, Ecum_t, R_t, Ia_t, Is_t, new_detections, new_E_t = outputs

    pm.Deterministic("S_t", S_t)

    # This is important for prevalence - the infected asymptomatic = presymptomatic
    pm.Deterministic("I_t", Ia_t + Is_t)
    pm.Deterministic("new_E_t", new_E_t)
    pm.Deterministic("E_t", E_t)
    pm.Deterministic("Ecum_t", Ecum_t)
    pm.Deterministic("R_t", R_t)
    pm.Deterministic("Is_t", Is_t)
    pm.Deterministic("Ia_t", Ia_t)
    pm.Deterministic("new_detections", new_detections)

    # Return the new symptomatic cases as the element to fit to data
    if return_all:
        return new_detections, E_t, Ia_t, Is_t, S_t
    else:
        return new_detections


def find_unobserved_days(new_cases, model, pop, model_settings):
    w=model_settings["unobserved_days_window_width"]
    max_non_zero_unobserved_days=model_settings["unobserved_days_max_non_zero_period"]
    threshold=model_settings["unobserved_days_threshold"]
    
    if(not "unobserved_days_zero_confirm_period" in pop):
        zero_confirm_days=model_settings["unobserved_days_zero_confirm_period_default"]

    else:
        zero_confirm_days=pop["unobserved_days_zero_confirm_period"]
    df=pd.DataFrame(index=new_cases.index)
    df['cases']=new_cases
    df['integrated_cases']=new_cases
    cases=df.cases.array
    integrated_cases=df.integrated_cases.array
    df['imputated_days']=1
    imputated_days=df.imputated_days.array
    df['lambda_r']=np.nan
    lambda_r=df.lambda_r.array
    #df['sum_err2_o_dof']=np.nan
    #sum_err2_o_dof=df.sum_err2_o_dof.array
    df['sigma_trend_lambda_r']=np.nan
    sigma_trend_lambda_r=df.sigma_trend_lambda_r.array
    df['sigma_lambda_r']=np.nan
    sigma_lambda_r=df.sigma_lambda_r.array
    df['z']=np.nan
    z=df.z.array
    df['m']=np.nan
    m_a=df.m.array
    df['include']=True
    include=df.include.array
    #sumerr2_o_dof = 1

    #def linear_regression(subdf, var_vec, old_sumerr2_o_dof, new_dt):
    def linear_regression(subdf, var_vec, new_dt):
        c = subdf.integrated_cases.array
        dt = subdf.imputated_days.array
        
        if(subdf.shape[0]==1):
            b = c[0] / float(dt[0])
            #return 0, b, 1, 0, 1. / dt[0], c, new_dt * b
            return 0, b, 0, 1. / dt[0], c, new_dt * b
        t = subdf.t.array
        subdf['sumt'] = (2*t - dt + 1) * dt * 0.5
        sumt = subdf.sumt.array
        sum_c_dt_o_var = (c * dt / var_vec).sum()
        sum_sumt_2_o_var = (sumt * sumt / var_vec).sum()
        sum_dt_2_o_var = (dt * dt / var_vec).sum()
        sum_sumt_dt_o_var = (sumt * dt / var_vec).sum()
        sum_sumt_c_o_var = (sumt * c / var_vec).sum()
        B = sum_sumt_2_o_var - sum_sumt_dt_o_var * sum_sumt_dt_o_var / sum_dt_2_o_var
        m = (sum_sumt_c_o_var - sum_sumt_dt_o_var * sum_c_dt_o_var / sum_dt_2_o_var) / B
        #m = 0
        b = (sum_c_dt_o_var - m * sum_sumt_dt_o_var) / sum_dt_2_o_var
        
        sumt_o_var = sumt / var_vec
            
        dmdc_vec = (sumt / var_vec - sum_sumt_dt_o_var * dt / var_vec / sum_dt_2_o_var) / B
        #dmdc_vec = 0
        dbdc_vec = (dt / (var_vec * var_vec) - dmdc_vec * sum_sumt_dt_o_var) / sum_dt_2_o_var
        
        #sumerr2 = ((c - m*sumt - b * dt)**2 / var_vec).sum()
        #sumerr2_o_dof = sumerr2 / (subdf.shape[0] - 2)
        #print(c)
        #print(sumt)
        #print(dt)
        #print(var_vec)
        #print(m,b)
        #print((c - m*sumt - b * dt)**2 / var_vec)
        #print("sumerr/dof: ", sumerr2_o_dof)
            
        #if(subdf.shape[0]==2 or sumerr2_o_dof < 1):
        #    sumerr2_o_dof = 1
            
        #only allow sumerr2_o_dof to increase if the last number of cases if above the trend, so we don't end up inflating sumerr2_o_dof due to missing partial observation
        #elif(sumerr2_o_dof > old_sumerr2_o_dof and (c[-1] < m*sumt[-1] + b * dt[-1])):
        #    sumerr2_o_dof = old_sumerr2_o_dof
            
        #print("new sumerr/dof: ", ((c - m*sumt - b * dt)**2 / (var_vec * sumerr2_o_dof)).sum() / (subdf.shape[0] - 2))
        #print(c)
        #print(sumt)
        #print(sum_c_o_var, sum_sumt_2_o_var, sum_dt_2_o_var, sum_sumt_dt_o_var, sum_sumt_c_o_var)
        #print("dmdc: ",dmdc_vec)
        #print(dbdc_vec)
        #print("m=", m, ", b=", b, ", dt=", imputated_days[r_loc])
        
        next_var_vec = m * sumt + b * dt
        #print(next_var_vec)
        next_lambda_var = new_dt * (m * (1 - new_dt) * 0.5 + b)
        #print("next lambda_var: ",next_lambda_var)
        #return m, b, sumerr2_o_dof, dmdc_vec, dbdc_vec, next_var_vec, next_lambda_var
        return m, b, dmdc_vec, dbdc_vec, next_var_vec, next_lambda_var

    for r in new_cases.index:
        #Counting adjacent unobserved or partially observed days.
        r_loc = df.index.get_loc(r)
        r_loc_minus_one = r_loc-1
        #print('r=', r, "(", r_loc,")")
        
        #Subset for the default date range used to compute lambda
        subdf=(df.loc[r-pd.Timedelta(days=w):r-pd.Timedelta(days=1)]).copy()
        #print(subdf)
        subdf['t']=np.arange(-subdf.shape[0],0)
        
        #Reduced subset after removing unobserved days
        subdf = subdf.loc[(subdf.include==True)]# | (subdf.t==-1)]
        
        if(subdf.shape[0]>0):  
            
            if(include[r_loc_minus_one]==False):
                imputated_days[r_loc] = imputated_days[r_loc_minus_one] + 1
                imputated_days[r_loc_minus_one] = 0
                integrated_cases[r_loc] += integrated_cases[r_loc_minus_one]
            var_vec = init_var_vec = subdf.integrated_cases.values #Vector of variances for the past integrated cases
            var_vec[var_vec<1]=1
            lambda_var = imputated_days[r_loc] #Variance for the new integrated cases
            #m, b, new_sumerr2_o_dof, dmdc_vec, dbdc_vec, next_var_vec, next_lambda_var = linear_regression(subdf, var_vec, sumerr2_o_dof, imputated_days[r_loc])
            m, b, dmdc_vec, dbdc_vec, next_var_vec, next_lambda_var = linear_regression(subdf, var_vec, imputated_days[r_loc])
            
            lambdar = -1
            
            while(abs(next_lambda_var - lambdar) > 0.1):
                lambdar = next_lambda_var
                next_var_vec = 0.5 * (var_vec + next_var_vec)
                var_vec = np.maximum(init_var_vec, next_var_vec)
                lambda_var = next_lambda_var if next_lambda_var > 1 else 1
                #m, b, new_sumerr2_o_dof, dmdc_vec, dbdc_vec, next_var_vec, next_lambda_var = linear_regression(subdf, var_vec, sumerr2_o_dof, imputated_days[r_loc])
                m, b, new_dmdc_vec, dbdc_vec, next_var_vec, next_lambda_var = linear_regression(subdf, var_vec, imputated_days[r_loc])
            m_a[r_loc]=m
            #lambdar = -(halfm * t[-1] + b) * t[-1]
            sumt_t0 = (1 - imputated_days[r_loc]) * imputated_days[r_loc] * 0.5
            lambdar = next_lambda_var
            #print("variables: ",dmdc_vec,"sumt: ",sumt_t0,"dbdc: ",dbdc_vec,"var_vec: ",var_vec)
            var_lambdar = ((dmdc_vec * sumt_t0 + imputated_days[r_loc] * dbdc_vec)**2 * var_vec).sum()
            z[r_loc] = (integrated_cases[r_loc] - lambdar) / np.sqrt(var_lambdar + lambda_var)
            #print(var_lambdar, lambda_var, sumerr2_o_dof)
            #include[r_loc] = ((df.index.array[r_loc].dayofweek==4 and include[r_loc_minus_one]==False) or stats.poisson.cdf(integrated_cases[r_loc], lambdar) > threshold)
            include[r_loc] = ((((integrated_cases[r_loc] > 0 and cases[r_loc] > 0) or cases[r_loc-zero_confirm_days:r_loc+1].sum()==0) and stats.norm.cdf(z[r_loc]) > threshold) or 
                                (np.amax(include[r_loc-max_non_zero_unobserved_days:r_loc])==False and cases[r_loc]>0))
            #print('lambda: ',lambdar, ' vs ', integrated_cases[r_loc], ' => include = ', include[r_loc])
            #lambdar = b
            #include[r_loc] = ((df.index.array[r_loc].dayofweek==4 and include[r_loc_minus_one]==False) or lambdar<=0 or stats.poisson.cdf(cases[r_loc], lambdar) > threshold)
            #print('lambda: ',lambdar, ' vs ', cases[r_loc], ' => include = ', include[r_loc])
                  
            #if(include[r_loc]):
            #    sumerr2_o_dof = new_sumerr2_o_dof
        else:
            lambdar = subdf.integrated_cases.sum() / subdf.imputated_days.sum()
            var_lambdar = np.nan
            lambda_var = np.nan
            include[r_loc] = True
    
            
        lambda_r[r_loc]=lambdar
        #sum_err2_o_dof[r_loc]=sumerr2_o_dof
        sigma_trend_lambda_r[r_loc]=np.sqrt(var_lambdar)
        sigma_lambda_r[r_loc]=np.sqrt(var_lambdar + lambda_var)

    #Forcefully include the last day
    #include[-1] = True
    df['corr_cases']=integrated_cases/imputated_days;
    df.reset_index(inplace=True)
    #List of indices for the observed days
    model.included_days=df.loc[df.include==True].index.values
    df['obs_day_index']=0
    #Adding observed day indices
    df.obs_day_index.array[model.included_days]=np.arange(model.included_days.shape[0])
    imputated_df=df.loc[(df.include==True) & (df.imputated_days>1)].imputated_days
    #Indices of the first reporting days following unobserved days. Remove last entry if the last day is assumed to be unobserved.
    model.next_reporting_days=imputated_df.index.values
    #Indices of the first reporting days following unobserved days, using observed day indices
    model.next_reporting_days_reduced=df.iloc[model.next_reporting_days].obs_day_index.values
    #The associated number of prior unobserved days
    model.n_unobserved_days=imputated_df.values-1
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    pd.set_option('display.expand_frame_repr', False)
    pd.set_option('max_colwidth', None)
    pd.set_option('display.width', 1000)

    log.info(str(df))
    #print(df)
    #print(integrated_cases[model.included_days])
    #print(model.included_days)
    #print(model.next_reporting_days)
    #print(model.next_reporting_days_reduced)
    #print(model.n_unobserved_days)

    if(include[-1] == True):
        log.info("Last day of data included")

    else:
        log.info("Last day of data excluded")

    model.new_cases_obs_reduced=df.integrated_cases[model.included_days].values

class PrevModel(Cov19Model):
    """Super class to represent point prevelance model

    This inherits from the Cov19Model class, which is developed by https://github.com/Priesemann-Group/covid19_inference
    """

    def __init__(
        self,
        new_cases_obs,
        data_begin,
        fcast_len,
        diff_data_sim,
        N_population,
        pop=None,
        settings=None,
        name="",
        model=None,
    ):

        # Initialize the base model (from covid_inference)
        super().__init__(
            new_cases_obs=new_cases_obs,
            data_begin=data_begin,
            fcast_len=fcast_len,
            diff_data_sim=diff_data_sim,
            N_population=N_population,
            name=name,
            model=model,
        )

        log.info(f"Building model for {pop['name']}")

        # apply change points, lambda is in log scale
        lambda_t_log = cov19.model.lambda_t_with_sigmoids(
            pr_median_lambda_0=pop["median_lambda_0"],
            pr_sigma_lambda_0=pop["sigma_lambda_0"],  # The initial spreading rate
            change_points_list=dynamicChangePoints(
                pop, settings["model"]
            ),  # these change points are periodic over time and inferred
        )

        # Probability of asymptomatic case
        settings["model"]["pa_upper"] = (
            settings["model"]["pa_mu"] + settings["model"]["pa_sigma"]
        )
        if settings["model"]["pa"] == "Beta":
            pa = pm.Beta(
                name="pa",
                alpha=settings["model"]["pa_a"],
                beta=settings["model"]["pa_b"],
            )
        elif settings["model"]["pa"] == "BoundedNormal":
            BoundedNormal = pm.Bound(
                pm.Normal,
                lower=settings["model"]["pa_lower"],
                upper=settings["model"]["pa_upper"],
            )
            pa = BoundedNormal(
                name="pa",
                mu=settings["model"]["pa_mu"],
                sigma=settings["model"]["pa_sigma"],
            )
        else:
            pa = pm.Uniform(name="pa", lower=0.15, upper=0.5)

        # Probability of undetected case
        if settings["model"]["pu"] == "BoundedNormal":
            BoundedNormal_pu = pm.Bound(
                pm.Normal,
                lower=settings["model"]["pu_a"],
                upper=settings["model"]["pu_b"],
            )
            pu = BoundedNormal_pu(
                name="pu",
                mu=(settings["model"]["pu_b"] - settings["model"]["pu_a"]) / 2
                + settings["model"]["pu_a"],
                sigma=0.2,
            )
        else:
            pu = pm.Uniform(
                name="pu",
                upper=settings["model"]["pu_b"],
                lower=settings["model"]["pu_a"],
            )

        mu = pm.Lognormal(
            name="mu",
            mu=np.log(1 / settings["model"]["asym_recover_mu_days"]),
            sigma=settings["model"]["asym_recover_mu_sigma"],
        )  # Asymptomatic infectious period until recovered
        mus = pm.Lognormal(
            name="mus",
            mu=np.log(1 / settings["model"]["sym_recover_mu_days"]),
            sigma=settings["model"]["sym_recover_mu_sigma"],
        )  # Pre-Symptomatic infectious period until showing symptoms -> isolated
        gamma = pm.Lognormal(
            name="gamma",
            mu=np.log(1 / settings["model"]["gamma_mu_days"]),
            sigma=settings["model"]["gamma_mu_sigma"],
        )

        # externally introduced cases
        Ein_t_log = None
        if "no_Ein" in pop and pop["no_Ein"]:
            Ein_t_log = None
        else:
            Ein_t_log = dynamicEin(self, new_cases_obs)

        new_Is_t = SEIRa(
            lambda_t_log,
            gamma,
            mu,
            mus,
            pa,
            pu,
            asym_ratio=settings["model"][
                "asym_ratio"
            ],  # 0.5 asymptomatic people are less infectious? - source CDC
            pr_Ia_begin=pop["pr_Ia_begin"],
            pr_Is_begin=pop["pr_Is_begin"],
            usePrevInit=settings["model"]["usePrevInit"],
            pr_prev_begin=settings["model"]["pr_prev_begin"],
            pr_E_begin=1,
            Ein_t_log=Ein_t_log,
            model=self,
        )

        new_cases_inferred_raw = cov19.model.delay_cases(
            cases=new_Is_t,
            pr_mean_of_median=pop["pr_delay_mean_of_median"],
        )
        pm.Deterministic("new_cases", new_cases_inferred_raw)

        # apply a weekly modulation, fewer reports during weekends
        if "noweekmod" in pop and pop["noweekmod"]:
            log.info("Not using weekly modulation")
            model_cases_inferred = new_cases_inferred_raw[self.diff_data_sim : self.diff_data_sim + self.data_len]
            new_cases_obs_reduced = new_cases_obs
        else:
            #new_cases_inferred = cov19.model.week_modulation(
            #    new_cases_inferred_raw,
            #    pr_mean_weekend_factor=pop["pr_mean_weekend_factor"],
            #    pr_sigma_weekend_factor=pop["pr_sigma_weekend_factor"],
            #    name_cases="new_cases",
            #)
            log.info("Using unobserved days imputation")
            find_unobserved_days(new_cases_obs, self, pop, settings["model"])

            if self.next_reporting_days.shape[0] > 0:
                log.info("%i unobserved day periods detected", self.next_reporting_days.shape[0])
                #For a given observed day that follows unobserved days, integrate the number of inferred cases up to the observed day, exclusively
                #The integrated values are returned in a reduced array containing only these values
                def integrated_inf(i, ired, n, infmod, inf):
                    return tt.set_subtensor(infmod[ired],inf[i-n:i].sum())

                #Scan the list of observed days that follow unobserved days, and compute the integrated number of inferred cases
                components, updates = theano.scan(fn=integrated_inf,
                                      sequences=[theano.shared(self.next_reporting_days), theano.tensor.arange(self.next_reporting_days.shape[0]), theano.shared(self.n_unobserved_days)],
                                      outputs_info=np.zeros_like(self.next_reporting_days),
                                      non_sequences=new_cases_inferred_raw)
                #Select only the observed days from new_cases_inferred_raw
                model_cases_inferred_noimp = new_cases_inferred_raw[self.diff_data_sim + self.included_days]
                #Impute the inferred cases from unobserved days
                model_cases_inferred = tt.inc_subtensor(model_cases_inferred_noimp[self.next_reporting_days_reduced],components[-1])

            else:
                log.info("No unobserved day detected")
                model_cases_inferred = new_cases_inferred_raw[self.diff_data_sim : self.diff_data_sim + self.data_len]
                new_cases_obs_reduced = new_cases_obs

        # TODO: better selector for when to use student-t vs normal
        use_st = False  # new_cases_obs.mean() > 60

        # Override
        if "normal_likelihood" in pop and pop["normal_likelihood"] == False:
            use_st = True

        # set the likelihood
        if use_st:

            sigma_obs = pm.HalfCauchy("sigma_obs", beta=30, shape=model.shape_of_regions)

            pm.StudentT(
                name="_new_cases_studentT",
                nu=4,
                mu=model_cases_inferred,
                sigma=tt.abs_(
                    model_cases_inferred + 1
                ) ** 0.5 * sigma_obs,
                observed=new_cases_obs_reduced,
            )
        else:
            pop["normal_likelihood"] = True
            log.info("using normal likelihood")
            #sigma_obs = pm.HalfCauchy("sigma_obs", beta=1)
            sigma_obs = pm.HalfNormal(name="sigma_obs", sigma=5)
            sigma_inferred = tt.abs_(model_cases_inferred + 1) ** 0.5 * sigma_obs
            pm.Normal(
                "new_cases_Norm",
                mu=model_cases_inferred,
                sigma=sigma_inferred,
                observed=self.new_cases_obs_reduced,
            )

